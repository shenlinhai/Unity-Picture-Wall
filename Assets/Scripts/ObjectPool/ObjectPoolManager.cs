﻿using System.Collections.Generic;
using UnityEngine;


public class ObjectPoolManager : ManagerBase
{
    /// <summary>
    /// 所有对象池储存
    /// </summary>
    private Dictionary<string, ObjectPool> m_PoolDic;

    /// <summary>
    /// 对象池根节点
    /// </summary>
    private Transform m_RootPoolTrans;

    private void Start()
    {
        m_PoolDic = new Dictionary<string, ObjectPool>();
        m_RootPoolTrans = this.gameObject.transform;
    }


    /// <summary>
    /// 创建一个新的对象池
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="poolName"></param>
    /// <returns></returns>
    public T CreateObjectPool<T>(string poolName) where T : ObjectPool, new()
    {
        if (m_PoolDic.ContainsKey(poolName))
        {
            return m_PoolDic[poolName] as T;
        }

        GameObject obj = new GameObject(poolName);
        obj.transform.SetParent(m_RootPoolTrans);
        T pool = new T();
        pool.Init(poolName, obj.transform);
        m_PoolDic.Add(poolName, pool);
        return pool;
    }

    public GameObject GetGameObject(string poolName, Vector3 position, float lifetTime)
    {
        if (m_PoolDic.ContainsKey(poolName))
        {
            return m_PoolDic[poolName].Get(position, lifetTime);
        }
        return null;
    }

    public void RemoveGameObject(string poolName, GameObject go)
    {
        if (m_PoolDic.ContainsKey(poolName))
        {
            m_PoolDic[poolName].Remove(go);
        }
    }

    // 销毁所有对象池
    public void Destroy()
    {
        m_PoolDic.Clear();
        GameObject.Destroy(m_RootPoolTrans);
    }
}
