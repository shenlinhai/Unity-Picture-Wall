﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour 
{
    /// <summary>
    /// 主相机
    /// </summary>
    private Transform _mainCamera;
    /// <summary>
    /// 最小距离
    /// </summary>
    private float _mindistance = -17.4f;
    /// <summary>
    /// 最大距离
    /// </summary>
    private float _maxdistance = 17.4f;

    private float _movedistance = 0;

    private void Start()
    {
        _mainCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    void Update ()
	{
        if (Input.GetMouseButton(1))
        {
            MianCameraMove();
        }
	}

    /// <summary>
    /// 相机视角左右移动
    /// </summary>
    void MianCameraMove()
    {
        _movedistance = _mainCamera.position.x - (Input.GetAxis("Mouse X") * Time.deltaTime);

        if (_movedistance >= _maxdistance)
        {
            _mainCamera.position = new Vector3(_maxdistance, _mainCamera.position.y, _mainCamera.position.z);
        }
        else if (_movedistance <= _mindistance)
        {
            _mainCamera.position = new Vector3(_mindistance, _mainCamera.position.y, _mainCamera.position.z);
        }
        else
        {
            _mainCamera.position = new Vector3(_movedistance, _mainCamera.position.y, _mainCamera.position.z);
        }
    }
}
