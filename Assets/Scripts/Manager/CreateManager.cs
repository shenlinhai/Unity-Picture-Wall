﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateManager : ManagerBase
{

    public event Action<bool> InitFinishedHander;
    /// <summary>
    /// 是否初始化完成
    /// </summary>
    private bool isInitFinished = false;
    public bool IsInitFinished {
        get
        {
            return isInitFinished;
        }
        set
        {
            isInitFinished = value;

            InitFinishedHander?.Invoke(value);
        }
    }

    #region Cube
    /// <summary>
    /// 1行的数量
    /// </summary>
    public int RowNum = 36;

    /// <summary>
    /// 行数
    /// </summary>
    public int RowCount = 8;

    /// <summary>
    /// 开始坐标
    /// </summary>
    public float StartPosX = -28;
    public float StartPosY = 5.5f;

    /// <summary>
    /// 间距X
    /// </summary>
    public float IntervalX = 1.6f;

    /// <summary>
    /// 间距Y
    /// </summary>
    public float IntervalY = 1.6f;

    public CubeInfo CubePrefab;

    /// <summary>
    /// 生成对象父物体
    /// </summary>
    public CubeAnimation CubeParent;

    #endregion



    public GameObject TextPrefab;


    //1.创建Cube,2创建文字,3.创建图片例子特效
    private void Start()
    {
        StartCoroutine(CreateCube());
    }

    /// <summary>
    /// 创建Cube
    /// </summary>
    /// <returns></returns>
    IEnumerator CreateCube()
    {
        for (int i = 0; i < RowCount; i++)
        {
            List<CubeInfo> tempList = new List<CubeInfo>();
            for (int j = 0; j < RowNum; j++)
            {
                CubeInfo go = Instantiate(CubePrefab, CubeParent.transform);

                //添加到Cube动画集合中
                CubeParent.AddCubeObj(go);
                tempList.Add(go);
                go.name = i + "*" + j;
                Vector3 localScale = go.transform.localScale;
                Vector3 startPos = new Vector3(StartPosX + ((localScale.x + IntervalX) * j), StartPosY - ((localScale.y + IntervalY) * i), 0);

                //赋值该对象的最终位置
                go.TargetPos = startPos;

                go.gameObject.SetActive(false);
                //go.transform.position = startPos;
                yield return null;
            }
            CubeParent.AddRowLineObj(tempList);
        }
        IsInitFinished = true;
    }
}
