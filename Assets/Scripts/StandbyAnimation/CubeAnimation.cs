﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public partial class CubeAnimation : ManagerBase
{
    /// <summary>
    /// Cube动画对象
    /// </summary>
    public List<CubeInfo> CubeObj = new List<CubeInfo>();

    /// <summary>
    /// 行列分离动画对象
    /// </summary>
    public List<List<CubeInfo>> RowLineObj = new List<List<CubeInfo>>();

    private CubeAnimationType cubeAnimationType;
    /// <summary>
    /// 当前Cube动画的动画类型
    /// </summary>
    public CubeAnimationType CubeAnimationType {
        get
        {
            return cubeAnimationType;
        }
        set
        {
            cubeAnimationType = value;
            SetCubeAnimationType(value);
        }
    }
    /// <summary>
    /// 对象生成
    /// </summary>
    private CreateManager createManager;

    /// <summary>
    /// Cube进场动画结束
    /// </summary>
    public event Action EnterCubeIsFinishedHander;

    /// <summary>
    /// Scale动画完成回调
    /// </summary>
    public event Action ScaleCubeIsFinishedHander;

    /// <summary>
    /// Position动画完成
    /// </summary>
    public event Action PositionCubeIsFinishedHander;

    /// <summary>
    /// 退出Cube动画完成
    /// </summary>
    public event Action ExitCubeIsFinishedHander;





    private void Start()
    {
        createManager = IOC.Resolve<CreateManager>();
        //StartCoroutine("ImitateUpdate");
        EnterCubeIsFinishedHander += AddInputManager;
    }

    /// <summary>
    /// 实时检测动画启动
    /// </summary>
    /// <returns></returns>
    IEnumerator ImitateUpdate()
    {
        while (true)
        {
            yield return new WaitUntil(() => createManager.IsInitFinished);
            //CubeAnimationType = CubeAnimationType.EnterCube;
            CubeAnimationEnter();
            StopCoroutine("ImitateUpdate");
        }
    }

    /// <summary>
    /// 启动动画
    /// </summary>
    public void CubeAnimationEnter()
    {
        CubeAnimationType = CubeAnimationType.EnterCube;
    }


    /// <summary>
    /// 添加预制Cube
    /// </summary>
    /// <param name="cubeObj"></param>
    public void AddCubeObj(CubeInfo cubeObj)
    {
        if (!CubeObj.Contains(cubeObj))
        {
            CubeObj.Add(cubeObj);
        }
    }

    /// <summary>
    /// 添加行列信息动画对象
    /// </summary>
    /// <param name="cubeInfo"></param>
    public void AddRowLineObj(List<CubeInfo> cubeInfo)
    {
        if (!RowLineObj.Contains(cubeInfo))
        {
            RowLineObj.Add(cubeInfo);
        }
    }
    
    void AddInputManager()
    {
        GameObject.Find("Manager").AddComponent<InputManager>();
    }

    public void SetCubeAnimationType(CubeAnimationType cubeAnimationType)
    {
        switch (cubeAnimationType)
        {
            case CubeAnimationType.None:
                break;
            case CubeAnimationType.EnterCube:
                EnterCubeAnimation();
                break;
            case CubeAnimationType.Scale:
                ScaleCubeAnimation();
                break;
            case CubeAnimationType.Position:
                PositionCubeAnimation();
                break;
            case CubeAnimationType.ExitCube:
                ExitCubeCubeAnimation();
                break;
            default:
                break;
        }
    }
    public void EnterCubeAnimation()
    {
        Debug.Log("开始播放进场动画");
        int index = UnityEngine.Random.Range(0, 2);
        //TODO随机
        EnterCubeAnimation((EnterCubeAnimationType)index);
    }

    public void ScaleCubeAnimation()
    {
        Debug.Log("开始播放缩放动画");
        ScaleCubeIsFinishedHander?.Invoke();
    }

    public void PositionCubeAnimation()
    {
        Debug.Log("开始播放位移动画");
        PositionCubeIsFinishedHander?.Invoke();
    }

    public void ExitCubeCubeAnimation()
    {
        Debug.Log("开始播放退场动画");
        ExitCubeIsFinishedHander?.Invoke();
    }

    /// <summary>
    /// 动画实现(5种进场动画,随机选择)
    /// </summary>
    /// <param name="animationStates"></param>
    public virtual void EnterCubeAnimation(EnterCubeAnimationType enterCubeAnimationType)
    {
        switch (enterCubeAnimationType)
        {
            case EnterCubeAnimationType.TopLeft:
                TopLeft(RowLineObj);
                break;
            case EnterCubeAnimationType.TopRight:
                TopRight(RowLineObj);
                break;
            case EnterCubeAnimationType.Center:
                Center(RowLineObj);
                break;
            case EnterCubeAnimationType.BottomLeft:
                BottomLeft(RowLineObj);
                break;
            case EnterCubeAnimationType.BottomRight:
                BottomRight(RowLineObj);
                break;
            default:
                break;
        }
    }

    private void OnDestroy()
    {
        EnterCubeIsFinishedHander -= AddInputManager;
    }
}
