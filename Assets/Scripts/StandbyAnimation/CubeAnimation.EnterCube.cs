﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class CubeAnimation
{
    public enum EnterCubeAnimationType
    {
        TopLeft = 0,
        TopRight,
        Center,
        BottomLeft,
        BottomRight
    }

    /// <summary>
    /// 左侧起始点坐标
    /// </summary>
    public float InvisibleXLeft = -30;

    /// <summary>
    /// 右侧起始点坐标
    /// </summary>
    public float InvisibleXRight = 30;

    /// <summary>
    /// 上起始点坐标
    /// </summary>
    public float InvisibleYTop = 7;

    /// <summary>
    /// 下起始点坐标
    /// </summary>
    public float InvisibleYBottom = -7;


    public void TopLeft(List<List<CubeInfo>> moveObj)
    {
        for (int i = 0; i < moveObj.Count; i++)
        {
            int indexI = i;
            List<CubeInfo> temp = moveObj[indexI];

            for (int j = temp.Count - 1; j >= 0; j--)
            {
                int indexJ = j;
                Vector3 targetPos = temp[indexJ].TargetPos;
                temp[indexJ].transform.localPosition = new Vector3(InvisibleXLeft, targetPos.y, targetPos.z);
                temp[indexJ].gameObject.SetActive(true); temp[indexJ].transform.DOLocalMoveX(targetPos.x, 1).SetDelay(Mathf.Abs(indexJ - temp.Count) * 0.1f + Mathf.Abs(indexJ - temp.Count) * 0.1f + UnityEngine.Random.Range(0.1f, 0.3f)).OnComplete(() => {
                    temp[indexJ].transform.DORotate(new Vector3(100, 100, 100), 1).SetLoops(2, LoopType.Yoyo)
                        .SetDelay(indexJ * 0.1f + (moveObj.Count - indexI) * 0.1f).OnComplete(() => {
                            if((indexI == (moveObj.Count - 1)) && (indexJ == 0)) EnterCubeIsFinishedHander?.Invoke();
                    });
                });
            }
        }
    }

    public void TopRight(List<List<CubeInfo>> moveObj)
    {
        for (int i = 0; i < moveObj.Count; i++)
        {
            int indexI = i;
            List<CubeInfo> temp = moveObj[indexI];

            for (int j = 0; j < temp.Count; j++)
            {
                int indexJ = j;
                Vector3 targetPos = temp[indexJ].TargetPos;
                temp[indexJ].transform.localPosition = new Vector3(InvisibleXRight, targetPos.y, targetPos.z);
                temp[indexJ].gameObject.SetActive(true);
                temp[indexJ].transform.DOLocalMoveX(targetPos.x, 1).SetDelay(j * 0.1f + j * 0.1f + UnityEngine.Random.Range(0.1f, 0.3f)).OnComplete(() => {
                    temp[indexJ].transform.DORotate(new Vector3(100, 100, 100), 1).SetLoops(2, LoopType.Yoyo).SetLoops(2, LoopType.Yoyo)
                       .SetDelay(j * 0.1f + (moveObj.Count - indexI) * 0.1f).OnComplete(() => {
                           if ((indexI == (moveObj.Count - 1)) && (indexJ == (temp.Count - 1))) EnterCubeIsFinishedHander?.Invoke();
                       });
                });
            }
        }
    }

    public void Center(List<List<CubeInfo>> moveObj)
    {
        //TODO
    }

    public void BottomLeft(List<List<CubeInfo>> moveObj)
    {
        //TODO
    }

    public void BottomRight(List<List<CubeInfo>> moveObj)
    {
        //TODO
    }
}
