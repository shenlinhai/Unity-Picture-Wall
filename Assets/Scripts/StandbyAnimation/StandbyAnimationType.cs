﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 动画类型
/// </summary>
public enum StandbyAnimationType
{
    /// <summary>
    /// Cube动画
    /// </summary>
    Cube,

    /// <summary>
    /// 文字动画
    /// </summary>
    Text,

    /// <summary>
    /// 粒子动画
    /// </summary>
    Particle
}

/// <summary>
/// Cube动画类型
/// </summary>
public enum CubeAnimationType
{
    /// <summary>
    /// 无动画
    /// </summary>
    None,

    /// <summary>
    /// Cube进场动画
    /// </summary>
    EnterCube,

    /// <summary>
    /// 缩放动画
    /// </summary>
    Scale,

    /// <summary>
    /// 位移动画
    /// </summary>
    Position,

    //TODO

    /// <summary>
    /// 离场动画
    /// </summary>
    ExitCube
}
