﻿using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    private static T m_Instance = null;

    public static T GetInstance {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new GameObject(typeof(T).Name).AddComponent<T>();
            }
            return m_Instance;
        }
    }

    public virtual void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this as T;
        }
    }
}
