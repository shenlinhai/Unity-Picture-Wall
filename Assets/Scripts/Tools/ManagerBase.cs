﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ManagerBase : MonoBehaviour
{
    protected virtual void Awake()
    {
        IOC.Register(this);
    }
}
