﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IOCContainer
{
    private class Item
    {
        public object Instance;
        public Item(object instance)
        {
            Instance = instance;
        }

        public T Resolve<T>()
        {
            if (Instance != null)
            {
                return (T)Instance;
            }
            return default;
        }
    }

    private Dictionary<Type, Item> m_registered = new Dictionary<Type, Item>();


    public void Register<T>(T instance)
    {
        if (instance == null)
        {
            throw new ArgumentNullException("instance");
        }
        if (m_registered.ContainsKey(instance.GetType()))
        {
            Debug.LogWarningFormat("type {0} already registered.", instance.GetType());
        }

        m_registered[instance.GetType()] = new Item(instance);
    }


    public void Unregister<T>(T instance)
    {
        Item item;
        if (m_registered.TryGetValue(instance.GetType(), out item))
        {
            if (ReferenceEquals(item.Instance, instance))
            {
                m_registered.Remove(instance.GetType());
            }
        }
    }

    public T Resolve<T>()
    {
        Item item;
        if (m_registered.TryGetValue(typeof(T), out item))
        {
            return item.Resolve<T>();
        }
        return default;
    }

    public void Clear()
    {
        m_registered.Clear();
    }
}

public static class IOC
{
    /// <summary>
    /// 注册
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="instance"></param>
    public static void Register<T>(T instance)
    {
        m_container.Register(instance);
    }

    /// <summary>
    /// 取消注册
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="instance"></param>
    public static void Unregister<T>(T instance)
    {
        m_container.Unregister(instance);
    }

    /// <summary>
    /// 获取
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T Resolve<T>()
    {
        return m_container.Resolve<T>();
    }

    private static IOCContainer m_container;

    static IOC()
    {
        m_container = new IOCContainer();
    }

    public static void ClearAll()
    {
        m_container.Clear();
    }
}