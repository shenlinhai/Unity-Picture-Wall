﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectTools
{
    public static bool MonitorTweenComplete(List<Tweener> lastTween)
    {
        bool isComplete = true;
        for (int i = 0; i < lastTween.Count; i++)
        {
            if (!lastTween[i].IsComplete())
            {
                isComplete = false;
            }
        }
        return isComplete;
    }
}
