﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    /// <summary>
    /// 启动动画UI对象
    /// </summary>
    public CanvasGroup LoaderTimer;

    private CreateManager createManager;

    private CubeAnimation cubeAnimation;

    void Start()
    {
        createManager = IOC.Resolve<CreateManager>();
        cubeAnimation = IOC.Resolve<CubeAnimation>();
        createManager.InitFinishedHander += SetLoaderTimeActive;
    }

    private void OnDestroy()
    {
        createManager.InitFinishedHander -= SetLoaderTimeActive;
    }

    /// <summary>
    /// 初始化完成回调
    /// </summary>
    /// <param name="isFinished"></param>
    public void SetLoaderTimeActive(bool isFinished)
    {
        if (isFinished)
        {
            LoaderTimer.DOFade(0, 1).OnComplete(() => {
                LoaderTimer.gameObject.SetActive(false);
                cubeAnimation.CubeAnimationEnter();
                LoaderTimer.alpha = 1;
            });
        }
    }
}
